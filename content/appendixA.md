## Exempel i CSV, kommaseparerad

<div class="example csvtext">
id,source,title,description,artform,creator,created,artmedium,artwork_surface,width,height,depth,street,city,region,latitude,longitude,URL,image,rights
<br>
983-23HE,2120001637,AI-nutid,Ett konsverk med framtiden i fokus,Skulptur,Johan Johansson,2023,,,180.5,,Skulptur,Johan Johansson,Bygatan 1,Gullspång,Västra Götaland,59.399237,16.417352,http://example.com/ai-nutid, http://example.com/ai-nutid.jpg, Fredrik Johnsson
</div>

