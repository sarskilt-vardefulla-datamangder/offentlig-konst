# RDF/XML exempel

``` xml
<?xml version="1.0"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:dc="http://purl.org/dc/elements/1.1/"
         xmlns:dcterms="http://purl.org/dc/terms/"
         xmlns:schema="http://schema.org/">
  <rdf:Description rdf:about="http://example.com/ai-nutid">
    <rdf:type rdf:resource="http://schema.org/VisualArtWork"/>
    <dcterms:identifier>983-23HE</dcterms:identifier>
    <dcterms:title xml:lang="sv">AI-nutid</dcterms:title>
    <dcterms:description xml:lang="sv">Ett konsverk med framtiden i fokus</dcterms:description>
    <schema:artform>Skulptur</schema:artform>
    <dcterms:creator>Johan Johansson</dcterms:creator>
    <dcterms:created>2023</dcterms:created>
    <schema:height rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">180.5</schema:height>
    <schema:streetAddress>Bygatan 1</schema:streetAddress>
    <schema:addressLocality>Gullspång</schema:addressLocality>
    <schema:addressRegion>Västra Götaland</schema:addressRegion>
    <schema:latitude rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">59.399237</schema:latitude>
    <schema:longitude rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">16.417352</schema:longitude>
    <schema:url rdf:resource="http://example.com/ai-nutid"/>
    <schema:image rdf:resource="http://example.com/ai-nutid.jpg"/>
    <dc:rights>Fredrik Johnsson</dc:rights>
  </rdf:Description>
</rdf:RDF>

