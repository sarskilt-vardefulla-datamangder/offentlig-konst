# Introduktion 

Denna specifikation anger vilka fält som är obligatoriska och rekommenderade. T.ex. gäller att man måste ange id, creator och title. 

I appendix A finns ett exempel på hur ett konstverk uttrycks i CSV. I appendix B uttrycks samma exempel i JSON och i appendix C uttrycks samma exempel i RDF/XML. 

Denna specifikation definierar en enkel tabulär informationsmodell för offentliga konstverk. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. 
