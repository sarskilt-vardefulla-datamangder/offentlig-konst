# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt ett konstverk och varje kolumn motsvarar en egenskap för det konstverket. 22 attribut är definierade, där de första 6 är obligatoriska. 

<div class="note" title="1">

Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.

</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**id**](#id)|1|text|**Obligatorisk** - Anger identifierare för konstverket.|
|[**source**](#source)|1|heltal|**Obligatorisk** - Anger organisationsnumret för förvaltande myndighet.|
|[**title**](#title)|1|text|**Obligatorisk** - Anger konstverkets titel.|
|[**description**](#description)|1|text|**Obligatorisk** - Anger beskrivning av konstverket.|
|[**artform**](#artform)|1|text|**Obligatorisk** - Anger konstverkets huvudkategori.|
|[**creator**](#creator)|1|text|**Obligatorisk** - Anger konstnären med förnamn följandes av efternamn. Exempel: Patrik Johansson. |
|[created](#created)|0..1|[dateTime](#dateTime)| År när konstverket skapades.|
|[artmedium](#artmedium)|0..*|text|Anger material som användes för konstverket.|
|[artwork_surface](#artmedium)|0..*|text|Anger stödjande material till konstverket.|
|[width](#width)|0..1|[decimal](#decimal)|Anger konstverkets bredd.|
|[height](#height)|0..1|[decimal](#decimal)|Anger konstverkets höjd.|
|[depth](#depth)|0..1|[decimal](#decimal)|Anger konstverkets djup.|
|[street](#street)|0..1|text|Anger gatuadress.|
|[city](#city)|0..1|text|Anger ort.|
|[region](#region)|0..1|text|Anger län.|
|[latitude](#latitude)|0..1|decimal|Anger latitud enligt WGS84.|
|[longitude](#longitude)|0..1|decimal|Anger longitud enligt WGS84.|
|[URL](#URL)|0..*|[URL](#url)|Anger en länk till ingångssida för mer information om konstverket.|
|[image](#image)|0..*|[URL](#url)| Anger en länk eller flera länkar till bilder på konstverket.|
|[rights](#rights)|0..1|text| Anger namnet på fotografen. |
|[license](#license)|0..1|[CC-BY](#license)| Anger vilka licensrättigheter som gäller för bilden/bilderna. |

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**

Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

Reguljärt uttryck: **`/^(https?|ftp):\/\/[^\s\/$.?#].[^\s]*$/`**

En länk till bild eller flera bilder på konstverket.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime**


Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonsegenskap. Om det fullständiga datumet är okänt kan månad och år (YYYYMM) eller bara år (YYYY) användas vilket är vanligt i fallen för konstverk.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värdet som kan anges för att beskriva ett år:

YYYY

Värdet som kan anges för att beskriva ett år och månad:

YYYYMM

## Förtydligande av attribut

### id 

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Ange identfierare för konstverket. Identiferare ska vara unikt och aldrig återanvändas för andra entiteter.

### source

Reguljärt uttryck: **`/^[1-9]\d*$/`**

Ange organisationsnummret utan mellanslag eller bindesstreck för den förvaltande myndigheten. Exempel för Gullspångs kommun: 2120001637.

### title

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange titeln på konstverket.

### description

Reguljärt uttryck: **`/^".*?"$/`**

Ange ytterligare information om konstverket som ett komplementerar resterande attribut i datamodellen. 

### artform 

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange huvudsaklig typ för konstverket. Exempelvis: Skulptur.

### creator

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange konstnären med förnamn och efternamn. Exempelvis: Johan Johansson.

### created

Reguljärt uttryck: **`/^(?!0{4})\d{4}$/`**

Ange året då konstverket skapades.

### artmedium 

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange konstverkets material. Exempelvis: akryl. 

*Vid specifikationens publicering fanns det inte strikta vokabulär att hämta värden från till detta attribut men kommer att inkorpereras direkt i denna specifikation när dessa finns på nationell eller internationell nivå.* 

### artwork_surface

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange konstverkets stödjande material. Exempelvis: canvas. 

*Vid specifikationens publicering fanns det inte strikta vokabulär att hämta värden från till detta attribut men kommer att inkorpereras direkt i denna specifikation när dessa finns på nationell eller internationell nivå.*

### width

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`** 

Ange konstverkets bredd. 

### height 

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`** 

Ange konstverkets höjd. 

### depth

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`** 

Ange konstverkets djup. 

### address 

Reguljärt uttryck: **`/^[a-zA-Z0-9 ]*$/`**

Ange gatuadress, exempelvis Folkungagatan 140.

### city

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange stad, exempelvis Göteborg.

### region

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange region, exempelvis Västra Götaland. 

### latitude

Reguljärt uttryck: **`/^-?\d+(\.\d+)?$/`** 

WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude 

Reguljärt uttryck: **`^-?\d+(\.\d+)?$`** 

Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.

### image 

Ange en länk eller flera länker till bild/bilder på konstverket. Ange inledande schemat https:// eller http://

### rights	

Reguljärt uttryck: **`/^[^'"]*$/`**

Ange namnet på fotografen eller organisationen som har tagit bilden på konstverket.

### license 

Ange licensen som gäller för bilden på konstverket och är det flera bilder så är det fördelaktigt att ha en licens för alla bilder. Specifikationen är tabulär och därav denna aspekt. 

Värden som kan anges från [Creative Commons](http://creativecommons.org) och då anger ni den fullständiga länken som värde i fältet license.

<br>
CC BY 4.0 (Attribution)	[http://creativecommons.org/licenses/by/4.0/](http://creativecommons.org/licenses/by/4.0/)
<br>
CC BY-NC 4.0 (Attribution, Non-Commercial) [http://creativecommons.org/licenses/by-nc/4.0/](http://creativecommons.org/licenses/by-nc/4.0/)
<br>
CC BY-NC-ND 4.0 (Attribution, Non-Commercial, No Derivative Works) [http://creativecommons.org/licenses/by-nc-nd/4.0/](http://creativecommons.org/licenses/by-nc-nd/4.0/)
<br>
CC BY-NC-SA 4.0 (Attribution, Non-Commercial, Share Alike) [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/)
<br>
CC BY-ND 4.0 (Attribution, No Derivative Works)	[http://creativecommons.org/licenses/by-nd/4.0/](http://creativecommons.org/licenses/by-nd/4.0/)
<br>
CC BY-SA 4.0 (Attribution, Share Alike)	[http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/)
<br>
CC0 1.0 (Public Domain Dedication, No Copyright) [http://creativecommons.org/publicdomain/zero/1.0/](http://creativecommons.org/publicdomain/zero/1.0/)
<br>








