**Bakgrund**

Syftet med denna specifikation är att beskriva information om konstverk på ett enhetligt och standardiserat vis. Arbetet har baserats på [applikationsprofil för offentliga konstverk](https://www.dataportal.se/sv/specifications/publicart/1#ref=?p=1&q=offentlig&s=2&t=20&f=&rt=spec_standard%24spec_profile&c=false).

Specifikationen syftar till att ge kommuner och regioner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver offentlig konst. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>
**[Riksantikvarieämbetet](https://www.raa.se)** - Domänkunskap och deltagande i arbetsgrupp <br>
**[Statens Konstråd](https://statenskonstrad.se/)** - Domänkunskap och deltagande i arbetsgrupp <br>



