# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "id":  "983-23HE",
    "source": "2120001637",
    "title": "AI-nutid",
    "description":  "Ett konsverk med framtiden i fokus",
    "artform":  "Skulptur",
    "creator":  "Johan Johansson",
    "created":  "2023",
    "artmedium": "",
    "artwork_surface": "",
    "width": "",
    "height": "180.5",
    "depth": "",
    "street":  "Bygatan 1",
    "city":  "Gullspång",
    "region":  "Västra Götaland",
    "latitude":  "59.399237",
    "longitude":  "16.417352",
    "URL":  "http://example.com/ai-nutid",
    "image":  "http://example.com/ai-nutid.jpg",
    "rights":  "Fredrik Johnsson"   
}
```
